function func1() {
    var input = document.getElementById('input');
    var findStr = document.getElementById('findStr');
    var indexStr = document.getElementById('indexStr');
    var output1 = document.getElementById('output1');
    if (input.value == "" || findStr.value == "" || indexStr.value == "") {
        alert('Input is empty!');
        return;
    }
    var ind = input.value.indexOf(findStr.value);
    if (ind == -1) {
        output1.innerHTML = "Using substring(): " + findStr.value + " not found!";
    }
    else {
        output1.innerHTML = "Using substring(): " + input.value.substring(4);
    }
    ind = input.value.indexOf(indexStr.value);
    if (ind == -1) {
        output1.innerHTML += "<br>Using indexOf(): " + indexStr.value + " not found!";
    }
    else {
        output1.innerHTML += "<br>Position of 'E' is " + input.value.indexOf('E').toString();
    }
}
