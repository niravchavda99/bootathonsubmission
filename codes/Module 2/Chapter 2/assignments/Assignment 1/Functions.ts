function func1() {
    let input : HTMLInputElement = <HTMLInputElement>document.getElementById('input');
    let findStr : HTMLInputElement = <HTMLInputElement>document.getElementById('findStr');
    let indexStr : HTMLInputElement = <HTMLInputElement>document.getElementById('indexStr');
    let output1 : HTMLInputElement = <HTMLInputElement>document.getElementById('output1');

    if(input.value == "" || findStr.value == "" || indexStr.value == "") {
        alert('Input is empty!');
        return;
    }

    let ind : number = input.value.indexOf(findStr.value);
    if(ind == -1) {
        output1.innerHTML = "Using substring(): " + findStr.value + " not found!";
    }
    else {
        output1.innerHTML = "Using substring(): " + input.value.substring(4);
    }

    ind = input.value.indexOf(indexStr.value);
    if(ind == -1) {
        output1.innerHTML += "<br>Using indexOf(): " + indexStr.value + " not found!";
    }
    else {
        output1.innerHTML += "<br>Position of 'E' is " + input.value.indexOf('E').toString();
    }

}