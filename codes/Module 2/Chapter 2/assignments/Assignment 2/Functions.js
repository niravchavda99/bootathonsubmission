function func2() {
    var input = document.getElementById('input');
    var output1 = document.getElementById('output1');
    var output2 = document.getElementById('output2');
    var output3 = document.getElementById('output3');
    if (input.value == "") {
        alert('Input is empty!');
        return;
    }
    var arr;
    var i;
    output1.innerHTML = input.value + " in uppercase: " + input.value.toUpperCase();
    output1.innerHTML += "<br>" + input.value + " in lowercase: " + input.value.toLowerCase();
    output1.innerHTML += "<br><br> <b>-> Array <-</b>";
    arr = input.value.split(" ");
    for (i = 0; i < arr.length; i++) {
        output1.innerHTML += "<br>" + arr[i];
    }
}
