function func2() {
    let input : HTMLInputElement = <HTMLInputElement>document.getElementById('input');
    let output1 : HTMLInputElement = <HTMLInputElement>document.getElementById('output1');
    let output2 : HTMLInputElement = <HTMLInputElement>document.getElementById('output2');
    let output3 : HTMLInputElement = <HTMLInputElement>document.getElementById('output3');

    if(input.value == "") {
        alert('Input is empty!');
        return;
    }

    let arr : string[];
    let i : number;

    output1.innerHTML = input.value + " in uppercase: " + input.value.toUpperCase();
    output1.innerHTML += "<br>" + input.value + " in lowercase: " + input.value.toLowerCase();

    output1.innerHTML += "<br><br> <b>-> Array <-</b>";
    arr = input.value.split(" ");

    for(i = 0; i < arr.length; i++) {
        output1.innerHTML += "<br>" + arr[i];
    }


}