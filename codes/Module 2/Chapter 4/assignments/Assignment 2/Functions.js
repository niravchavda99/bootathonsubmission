//Function to check valid input
function isValid(param) {
    return (param.value != "") && (!isNaN(parseInt(param.value)));
}
//A number is an Armstrong Number if it is equal to the
//sum of its own digits raised to the power of the number of digits.
//Function to check if number is armstrong or not
function isArmstrong(num) {
    var digits = num.toString().length; //Number of digits of the number
    var numberAfter = 0; //Result stored here
    var numberBefore = num;
    var remainder;
    while (num) { //Iterate until number becomes 0
        remainder = num % 10; //Taking the last digit of the number
        numberAfter += Math.pow(remainder, digits); //Each digit raised to no. of digits of the number
        num = Math.floor(num / 10); //removing the last digit of the number
    }
    return numberBefore == numberAfter; // Return true if number is armstrong
}
function armstrong() {
    var input = document.getElementById('input');
    var output = document.getElementById('output');
    if (!isValid(input)) { //If input is not valid alert the user
        alert('Invalid Input');
        return;
    }
    var n = parseInt(input.value);
    var i = 1;
    output.innerHTML = "<b>Armstrong numbers between 1 and " + n.toString() + " are: </b>";
    while (i <= n) {
        if (isArmstrong(i)) {
            output.innerHTML += i.toString() + ", ";
        }
        ++i;
    }
}
