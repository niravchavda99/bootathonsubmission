//Function to check valid input
function isValid(param : HTMLInputElement) : boolean {
    return (param.value != "") && (!isNaN(parseInt(param.value)));
}

//A number is an Armstrong Number if it is equal to the
//sum of its own digits raised to the power of the number of digits.
//Function to check if number is armstrong or not
function isArmstrong(num : number) : boolean {
    let digits : number = num.toString().length; //Number of digits of the number
    let numberAfter : number = 0; //Result stored here
    let numberBefore : number = num;
    let remainder : number;
    while(num) { //Iterate until number becomes 0
        remainder = num % 10; //Taking the last digit of the number
        numberAfter += Math.pow(remainder, digits); //Each digit raised to no. of digits of the number
        num = Math.floor(num / 10); //removing the last digit of the number
    }
    return numberBefore == numberAfter; // Return true if number is armstrong
}

function armstrong() {
    let input : HTMLInputElement = <HTMLInputElement> document.getElementById('input');
    let output : HTMLInputElement = <HTMLInputElement> document.getElementById('output');

    if(!isValid(input)) { //If input is not valid alert the user
        alert('Invalid Input');
        return;
    }

    let n : number = parseInt(input.value);
    let i : number = 1;

    output.innerHTML = "<b>Armstrong numbers between 1 and " + n.toString() + " are: </b>";

    while(i <= n) {
        if(isArmstrong(i)) {
            output.innerHTML += i.toString() + ", ";
        }

        ++i;
    }
}