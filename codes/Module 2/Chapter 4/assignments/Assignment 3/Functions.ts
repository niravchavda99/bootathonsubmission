//Function to check valid input
function isValid(param : HTMLInputElement) : boolean {
    return (param.value != "") && (!isNaN(+param.value));
}

//Function to generate table
function generateTable() {
    let input : HTMLInputElement = <HTMLInputElement> document.getElementById('input');
    let table : HTMLTableElement = <HTMLTableElement> document.getElementById('tb1');

    if(!isValid(input)) { //If input is not valid alert the user
        alert('Invalid Input!');
        return;
    }

    let count : number = 1;
    let num : number = +input.value;

    while(table.rows.length > 1) { //Deleting Existing Rows
        table.deleteRow(1);
    }

    for(count = 1; count <= num; count++) {
        let row : HTMLTableRowElement = table.insertRow();
        var cell : HTMLTableDataCellElement = row.insertCell();
        var text : HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = count.toString();
        cell.appendChild(text);

        var cell : HTMLTableDataCellElement = row.insertCell();
        var text : HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = num.toString() + " * " + count.toString() + " = ";
        cell.appendChild(text);

        var cell : HTMLTableDataCellElement = row.insertCell();
        var text : HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = (count * num).toString();
        cell.appendChild(text);
    }

}