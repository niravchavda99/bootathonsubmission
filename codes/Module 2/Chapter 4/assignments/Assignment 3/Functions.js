//Function to check valid input
function isValid(param) {
    return (param.value != "") && (!isNaN(+param.value));
}
//Function to generate table
function generateTable() {
    var input = document.getElementById('input');
    var table = document.getElementById('tb1');
    if (!isValid(input)) { //If input is not valid alert the user
        alert('Invalid Input!');
        return;
    }
    var count = 1;
    var num = +input.value;
    while (table.rows.length > 1) { //Deleting Existing Rows
        table.deleteRow(1);
    }
    for (count = 1; count <= num; count++) {
        var row = table.insertRow();
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = count.toString();
        cell.appendChild(text);
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = num.toString() + " * " + count.toString() + " = ";
        cell.appendChild(text);
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = (count * num).toString();
        cell.appendChild(text);
    }
}
