//Function to check valid input
function isValid(param) {
    return (param.value != "") && (!isNaN(+param.value));
}
function userInput() {
    var input = document.getElementById('input');
    var output = document.getElementById('output');
    if (!isValid(input)) { //If input is not valid alert the user
        alert('Invalid Input');
    }
    var n = +input.value;
    var i = 0;
    var positive = 0;
    var negative = 0;
    var zeros = 0;
    while (i < n) {
        var num = prompt('Enter number' + (i + 1).toString());
        var t = +num;
        if (num == "") { //If number not entered, re enter
            continue;
        }
        if (isNaN(t)) { //If number not entered, show error and re enter
            alert('Invalid number, Enter again...');
            continue;
        }
        (t > 0) ? ++positive : (t < 0) ? ++negative : ++zeros; //Using ternary operator
        ++i;
    }
    output.innerHTML = "There are " + positive.toString() + " positive numbers, ";
    output.innerHTML += negative.toString() + " negative numbers, and ";
    output.innerHTML += zeros.toString() + " zeros.";
}
