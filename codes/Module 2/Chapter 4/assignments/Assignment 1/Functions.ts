//Function to check valid input
function isValid(param : HTMLInputElement) : boolean {
    return (param.value != "") && (!isNaN(+param.value));
}

function userInput() {
    let input : HTMLInputElement = <HTMLInputElement>document.getElementById('input');
    let output : HTMLInputElement = <HTMLInputElement>document.getElementById('output');

    if(!isValid(input)) { //If input is not valid alert the user
        alert('Invalid Input');
    }
    let n : number = +input.value;
    let i : number = 0;
    let positive : number = 0;
    let negative : number = 0;
    let zeros : number = 0;

    while(i < n) {
        var num : any = prompt('Enter number' + (i + 1).toString());
        let t : number = +num;
        if(num == "") { //If number not entered, re enter
            continue;
        }
        if(isNaN(t)) { //If number not entered, show error and re enter
            alert('Invalid number, Enter again...');
            continue;
        }

        (t > 0) ? ++positive : (t < 0) ? ++negative : ++zeros; //Using ternary operator
        ++i;
        
    }

    output.innerHTML = "There are " + positive.toString() + " positive numbers, ";
    output.innerHTML += negative.toString() + " negative numbers, and ";
    output.innerHTML += zeros.toString() + " zeros.";
}