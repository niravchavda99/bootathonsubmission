//Function to check valid input
function isValid(param) {
    return (param.value != "") && (!isNaN(+param.value));
}
function calculate() {
    var a = document.getElementById('s1');
    var b = document.getElementById('s2');
    var c = document.getElementById('s3');
    if (!isValid(a) || !isValid(b) || !isValid(c)) { //If input is not valid alert the user
        alert('Invalid Input!');
        return;
    }
    var l1 = document.getElementById('l1');
    var l2 = document.getElementById('l2');
    var x = +a.value;
    var y = +b.value;
    var z = +c.value;
    l2.innerHTML = "";
    if ((x == y) && (y == z)) { //If all sides are equal, it is equilateral
        l1.innerHTML = "It is an equilateral triangle.";
    }
    else if ((x == y) || (y == z) || (x == z)) { //If not equilateral, there may be 2 equal sides
        l1.innerHTML = "It is an isosceles triangle.";
        if ((Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2)) || (Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2)) || (Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2)))
            l2.innerHTML = "It is also a right angle triangle.";
    }
    else if ((x != y) && (y != z) && (x != z)) { //If all sides are not equal, scalene
        l1.innerHTML = "It is an scalene triangle.";
        if ((Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2)) || (Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2)) || (Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2)))
            l2.innerHTML = "It is also a right angle triangle.";
    }
}
