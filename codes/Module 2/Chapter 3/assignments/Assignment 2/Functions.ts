//Function to check valid input
function isValid(param : HTMLInputElement) : boolean {
    return (param.value != "") && (!isNaN(+param.value));
}

function calculate() {
    let a : HTMLInputElement = <HTMLInputElement>document.getElementById('s1');
    let b : HTMLInputElement = <HTMLInputElement>document.getElementById('s2');
    let c : HTMLInputElement = <HTMLInputElement>document.getElementById('s3');

    if(!isValid(a) || !isValid(b) || !isValid(c)) {  //If input is not valid alert the user
        alert('Invalid Input!');
        return;
    }

    let l1 : HTMLInputElement = <HTMLInputElement>document.getElementById('l1');
    let l2 : HTMLInputElement = <HTMLInputElement>document.getElementById('l2');

    let x : number = +a.value;
    let y : number = +b.value;
    let z : number = +c.value;

    l2.innerHTML = "";


    if((x == y) && (y == z)) { //If all sides are equal, it is equilateral
        l1.innerHTML = "It is an equilateral triangle.";
    }
    else if((x == y) || (y == z) || (x == z)) { //If not equilateral, there may be 2 equal sides
        l1.innerHTML = "It is an isosceles triangle.";
        if((Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2)) || (Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2)) || (Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2)))
            l2.innerHTML = "It is also a right angle triangle.";
    }
    else if((x != y) && (y != z) && (x != z)) { //If all sides are not equal, scalene
        l1.innerHTML = "It is an scalene triangle.";
        if((Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2)) || (Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2)) || (Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2)))
            l2.innerHTML = "It is also a right angle triangle.";
    }
}