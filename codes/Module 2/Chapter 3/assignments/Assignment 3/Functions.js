function findRealAndImag() {
    var a = document.getElementById('n1');
    var b = document.getElementById('n2');
    var c = document.getElementById('n3');
    var data = a.value;
    var real;
    var imag;
    var i = data.indexOf("+");
    if (i == -1)
        i = data.indexOf("-");
    if (i != -1) {
        real = +data.substring(0, i);
        b.value = "Real Part: " + real;
        c.value = "Imag Part: " + data.substring(i, data.length - 1);
    }
    else {
        b.value = "Real Part: " + a.value;
        c.value = "Imag Part: 0";
    }
}
