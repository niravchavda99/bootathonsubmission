function findRealAndImag() {
    let a : HTMLInputElement = <HTMLInputElement>document.getElementById('n1');
    let b : HTMLInputElement = <HTMLInputElement>document.getElementById('n2');
    let c : HTMLInputElement = <HTMLInputElement>document.getElementById('n3');

    let data : string = a.value;
    let real : number;
    let imag : number;

    let i : number = data.indexOf("+");

    if(i == -1) 
        i = data.indexOf("-");

    if(i != -1) {
        real = +data.substring(0, i);
        b.value = "Real Part: " + real;
        c.value = "Imag Part: " + data.substring(i, data.length - 1);
    }
    else {
        b.value = "Real Part: " + a.value;
        c.value = "Imag Part: 0";
    }
}