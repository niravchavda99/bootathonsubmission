//Function to check valid input
function isValid(param) {
    return (param.value != "") && (!isNaN(+param.value));
}
function triangleArea(x1, y1, x2, y2, x3, y3) {
    return Math.abs(((x1 * (y2 - y3)) + (x2 * (y3 - y1)) + (x3 * (y1 - y2))) / 2);
}
function checkCoordinates() {
    var a1 = document.getElementById('x1');
    var a2 = document.getElementById('y1');
    var b1 = document.getElementById('x2');
    var b2 = document.getElementById('y2');
    var c1 = document.getElementById('x3');
    var c2 = document.getElementById('y3');
    var p1 = document.getElementById('x');
    var p2 = document.getElementById('y');
    //If input is not valid alert the user
    if (isValid(a1) && isValid(a2) && isValid(b1) && isValid(b2) && isValid(c1) && isValid(c2) && isValid(p1) && isValid(p2)) {
        var x1 = +a1.value;
        var y1 = +a2.value;
        var x2 = +b1.value;
        var y2 = +b2.value;
        var x3 = +c1.value;
        var y3 = +c2.value;
        var x = +p1.value;
        var y = +p2.value;
        var A_ABC = triangleArea(x1, y1, x2, y2, x3, y3);
        var A_PAB = triangleArea(x, y, x1, y1, x2, y2);
        var A_PBC = triangleArea(x, y, x2, y2, x3, y3);
        var A_PAC = triangleArea(x, y, x1, y1, x3, y3);
        var sum = A_PAB + A_PBC + A_PAC;
        if (Math.abs(A_ABC - sum) < 0.0000001)
            alert('Point lies inside the triangle!');
        else
            alert('Point doesn\'t lie inside the triangle!');
    }
    else {
        alert('Invalid Coordinates provided!');
    }
}
