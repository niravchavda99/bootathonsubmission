//Function to check valid input
function isValid(param : HTMLInputElement) : boolean {
    return (param.value != "") && (!isNaN(+param.value));
}

function triangleArea(x1 : number, y1 : number, x2 : number, y2 : number, x3 : number, y3 : number) : number{
    return Math.abs(((x1 * (y2 - y3)) + (x2 * (y3 - y1)) + (x3 * (y1 - y2))) / 2);
}

function checkCoordinates() {
    let a1 : HTMLInputElement = <HTMLInputElement>document.getElementById('x1');
    let a2 : HTMLInputElement = <HTMLInputElement>document.getElementById('y1');

    let b1 : HTMLInputElement = <HTMLInputElement>document.getElementById('x2');
    let b2 : HTMLInputElement = <HTMLInputElement>document.getElementById('y2');
    
    let c1 : HTMLInputElement = <HTMLInputElement>document.getElementById('x3');
    let c2 : HTMLInputElement = <HTMLInputElement>document.getElementById('y3');

    let p1 : HTMLInputElement = <HTMLInputElement>document.getElementById('x');
    let p2 : HTMLInputElement = <HTMLInputElement>document.getElementById('y');

    //If input is not valid alert the user
    if(isValid(a1) && isValid(a2) && isValid(b1) && isValid(b2) && isValid(c1) && isValid(c2) && isValid(p1) && isValid(p2)) {
        let x1 : number = +a1.value;
        let y1 : number = +a2.value;

        let x2 : number = +b1.value;
        let y2 : number = +b2.value;

        let x3 : number = +c1.value;
        let y3 : number = +c2.value;

        let x : number = +p1.value;
        let y : number = +p2.value;

        let A_ABC : number = triangleArea(x1, y1, x2, y2, x3, y3);
        let A_PAB : number = triangleArea(x, y, x1, y1, x2, y2);
        let A_PBC : number = triangleArea(x, y, x2, y2, x3, y3);
        let A_PAC : number = triangleArea(x, y, x1, y1, x3, y3);

        let sum = A_PAB + A_PBC + A_PAC;

        if(Math.abs(A_ABC - sum) < 0.0000001)
            alert('Point lies inside the triangle!');
        else
            alert('Point doesn\'t lie inside the triangle!');
    }
    else {
        alert('Invalid Coordinates provided!');
    }
}