//Function to check valid input
function isValid(param) {
    return (param.value != "") && (!isNaN(+param.value));
}
function checkOddEven() {
    var input = document.getElementById('input');
    var output = document.getElementById('output');
    var n = +input.value;
    if (!isValid(input)) { //If input is not valid alert the user
        alert('Invalid Input');
        return;
    }
    if (n % 2 == 0)
        output.innerHTML = n.toString() + " is even.";
    else
        output.innerHTML = n.toString() + " is odd.";
}
