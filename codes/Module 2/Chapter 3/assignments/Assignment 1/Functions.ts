//Function to check valid input
function isValid(param : HTMLInputElement) : boolean {
    return (param.value != "") && (!isNaN(+param.value));
}

function checkOddEven() {
    let input : HTMLInputElement = <HTMLInputElement>document.getElementById('input');
    let output : HTMLInputElement = <HTMLInputElement>document.getElementById('output');

    let n : number = +input.value;

    if(!isValid(input)) { //If input is not valid alert the user
        alert('Invalid Input');
        return;
    }

    if(n % 2 == 0)
        output.innerHTML = n.toString() + " is even.";
    else
        output.innerHTML = n.toString() + " is odd.";

}