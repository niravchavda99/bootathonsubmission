//Function to check valid input
function isValid(param) {
    return (param.value != "") && (!isNaN(+param.value));
}
function area() {
    var a1 = document.getElementById('a1');
    var a2 = document.getElementById('a2');
    var b1 = document.getElementById('b1');
    var b2 = document.getElementById('b2');
    var c1 = document.getElementById('c1');
    var c2 = document.getElementById('c2');
    var answer = document.getElementById('answer');
    //If input is not valid alert the user
    if (!isValid(a1) || !isValid(a2) || !isValid(b1) || !isValid(b2) || !isValid(c1) || !isValid(c2)) {
        alert('Invalid Input');
        return;
    }
    var x1 = parseFloat(a1.value);
    var y1 = parseFloat(a2.value);
    var x2 = parseFloat(b1.value);
    var y2 = parseFloat(b2.value);
    var x3 = parseFloat(c1.value);
    var y3 = parseFloat(c2.value);
    var a = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
    console.log(a);
    var b = Math.sqrt(Math.pow((x1 - x3), 2) + Math.pow((y1 - y3), 2));
    console.log(b);
    var c = Math.sqrt(Math.pow((x3 - x2), 2) + Math.pow((y3 - y2), 2));
    console.log(c);
    var s = (a + b + c) / 2;
    var area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
    answer.innerHTML = "Answer is: <br>" + area.toString();
}
