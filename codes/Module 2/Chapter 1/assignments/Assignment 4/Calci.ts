let n1 : HTMLInputElement = <HTMLInputElement> document.getElementById('i1');
let n2 : HTMLInputElement = <HTMLInputElement> document.getElementById('i2');
let answer : HTMLInputElement = <HTMLInputElement> document.getElementById('answer');

let a : number;
let b : number;

//Function to check valid input
function isValid(param : HTMLInputElement) : boolean {
    return (param.value != "") && (!isNaN(+param.value));
}

function add() {
    if(!isValid(n1)) { //If input is not valid alert the user
        alert('Invalid value for Number 1');
    }
    else if(!isValid(n2)) { //If input is not valid alert the user
        alert('Invalid value for Number 2');
    }
    else {
        a = +n1.value;
        b = +n2.value;
        answer.value = a.toString() + " + " + b.toString() + " = " + (a + b).toString();
    }
}

function subtract() {
    if(!isValid(n1)) { //If input is not valid alert the user
        alert('Invalid value for Number 1');
    }
    else if(!isValid(n2)) { //If input is not valid alert the user
        alert('Invalid value for Number 2');
    }
    else {
        a = +n1.value;
        b = +n2.value;
        answer.value = a.toString() + " - " + b.toString() + " = " + (a - b).toString();
    }
}

function multiply() {
    if(!isValid(n1)) { //If input is not valid alert the user
        alert('Invalid value for Number 1');
    }
    else if(!isValid(n2)) { //If input is not valid alert the user
        alert('Invalid value for Number 2');
    }
    else {
        a = +n1.value;
        b = +n2.value;
        answer.value = a.toString() + " * " + b.toString() + " = " + (a * b).toString();
    }
}

function divide() {
    if(!isValid(n1)) { //If input is not valid alert the user
        alert('Invalid value for Number 1');
    }
    else if(!isValid(n2)) { //If input is not valid alert the user
        alert('Invalid value for Number 2');
    }
    else {
        a = +n1.value;
        b = +n2.value;
        if(b == 0) {
            alert('Cannot divide by 0');
            return;
        }
        answer.value = a.toString() + " / " + b.toString() + " = " + (a / b).toString();
    }
}

function sinine() { 
    if(!isValid(n1)) { //If input is not valid alert the user
        alert('Invalid value!');
    }
    else {
        a = +n1.value;
        let t : HTMLInputElement = <HTMLInputElement> document.getElementById('mode');
        let mode : string = t.value;
        if(mode == "d") { //If mode is degree, convert to radians
            b = Math.PI / 180 * a;
        }
        answer.value = "sin(" + a.toString() + ") = " + Math.sin(b).toString();
    }
}

function cosinine() {
    if(!isValid(n1)) { //If input is not valid alert the user
        alert('Invalid value!');
    }
    else {
        a = +n1.value;
        let t : HTMLInputElement = <HTMLInputElement> document.getElementById('mode');
        let mode : string = t.value;
        if(mode == "d") { //If mode is degree, convert to radians
            b = Math.PI / 180 * a;
        }
        answer.value = "cos(" + a.toString() + ") = " + Math.cos(b).toString();
    }
}

function tangent() {
    if(!isValid(n1)) { //If input is not valid alert the user
        alert('Invalid value!');
    }
    else {
        a = +n1.value;
        let t : HTMLInputElement = <HTMLInputElement> document.getElementById('mode');
        let mode : string = t.value;
        if(mode == "d") { //If mode is degree, convert to radians
            b = Math.PI / 180 * a;
        }
        answer.value = "tan(" + a.toString() + ") = " + Math.tan(b).toString();
    }
}

function squareroot() {
    if(!isValid(n1)) { //If input is not valid alert the user
        alert('Invalid value!');
    }
    else {
        a = +n1.value;
        if(a < 0) {
            alert('Cannot find square root of a negative number!');
            return;
        }
        b = Math.sqrt(a);
        answer.value = "√" + a.toString() + " = " + b.toString();
    }
}

function power() {
    if(!isValid(n1)) { //If input is not valid alert the user
        alert('Invalid value for Number 1');
    }
    else if(!isValid(n2)) { //If input is not valid alert the user
        alert('Invalid value for Number 2');
    }
    else {
        a = +n1.value;
        b = +n2.value;
        answer.value = a.toString() + "^" + b.toString() + " = " + Math.pow(a, b).toString();
    }
}