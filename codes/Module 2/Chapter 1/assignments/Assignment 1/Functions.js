function circleArea() {
    var input = document.getElementById('input');
    var output = document.getElementById('output');
    if (input.value == "") { //Show error if input is empty
        alert('Enter the radius!');
        return;
    }
    var radius = +input.value;
    if (isNaN(radius)) { // show error if input is not a number
        alert('Invalid Radius!');
        return;
    }
    output.value = "Area of circle is " + (Math.PI * radius * radius).toString();
}
