function x_cosx()
{
    let input : HTMLInputElement = <HTMLInputElement> document.getElementById('input');
    let output : HTMLInputElement = <HTMLInputElement> document.getElementById('output');

    if(input.value == "") { //Show error if input is empty
        alert('Enter a value!');
        return;
    }

    let x : number = +input.value;

    if(isNaN(x)) { //Show error if input is not a number
        alert('Invalid value!');
        return;
    }

    output.value = "Answer is " + (x + Math.cos(Math.PI / 180 * x)).toString();

}