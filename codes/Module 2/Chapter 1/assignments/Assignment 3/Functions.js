function x_cosx() {
    var input = document.getElementById('input');
    var output = document.getElementById('output');
    if (input.value == "") { //Show error if input is empty
        alert('Enter a value!');
        return;
    }
    var x = +input.value;
    if (isNaN(x)) { //Show error if input is not a number
        alert('Invalid value!');
        return;
    }
    output.value = "Answer is " + (x + Math.cos(Math.PI / 180 * x)).toString();
}
