function sin1() {
    let t11 : HTMLInputElement = <HTMLInputElement>document.getElementById('t11');
    let t12 : HTMLInputElement = <HTMLInputElement>document.getElementById('t12');

    let a : number = Math.PI / 180 * parseFloat(t11.value);
    let b : number = Math.sin(a);

    t12.value = b.toString();
}

function cos1() {
    let t21 : HTMLInputElement = <HTMLInputElement>document.getElementById('t21');
    let t22 : HTMLInputElement = <HTMLInputElement>document.getElementById('t22');

    let a : number = Math.PI / 180 * parseFloat(t21.value);
    console.log(a);
    let b : number = Math.cos(a);
    console.log(Math.cos(a));
    t22.value = b.toString();
}

function area() {
    let a1:HTMLInputElement = <HTMLInputElement>document.getElementById('a1');
    let a2:HTMLInputElement = <HTMLInputElement>document.getElementById('a2');
    let b1:HTMLInputElement = <HTMLInputElement>document.getElementById('b1');
    let b2:HTMLInputElement = <HTMLInputElement>document.getElementById('b2');
    let c1:HTMLInputElement = <HTMLInputElement>document.getElementById('c1');
    let c2:HTMLInputElement = <HTMLInputElement>document.getElementById('c2');
    let answer:HTMLInputElement = <HTMLInputElement>document.getElementById('answer');

    var x1 : number = parseFloat(a1.value);
    var y1 : number = parseFloat(a2.value);
    var x2 : number = parseFloat(b1.value);
    var y2 : number = parseFloat(b2.value);
    var x3 : number = parseFloat(c1.value);
    var y3 : number = parseFloat(c2.value);

    var a = Math.sqrt(Math.pow((x2-x1), 2) + Math.pow((y2-y1), 2));
    console.log(a);
    var b = Math.sqrt(Math.pow((x1-x3), 2) + Math.pow((y1-y3), 2));
    console.log(b);
    var c = Math.sqrt(Math.pow((x3-x2), 2) + Math.pow((y3-y2), 2));
    console.log(c);

    var s = (a + b + c) / 2;

    var area = Math.sqrt(s * (s - a) * (s - b) * (s - c));

    answer.innerHTML = "Answer is: <br>" + area.toString();

}

function areaOfCircle() {
    let radius : HTMLInputElement = <HTMLInputElement>document.getElementById('r');
    let answer : HTMLInputElement = <HTMLInputElement>document.getElementById('answer');
    let r : number = parseFloat(radius.value);

    let a : number = Math.PI * Math.pow(r, 2);

    answer.innerHTML = "Area of circle with radius " + r.toString() + " is " + a.toString();
}