function comparision() {
    var a = document.getElementById('d1');
    var b = document.getElementById('d2');
    var x = +a.value;
    var y = +b.value;
    if (x > y)
        alert(x.toString() + " is greater than " + y.toString());
    else if (x < y)
        alert(x.toString() + " is less than " + y.toString());
    else
        alert("Numbers are same");
}
function check() {
    var a = document.getElementById('d1');
    var data = +a.value;
    if (isNaN(data))
        alert('Not a Number!');
    else
        alert('Yureka!, A Number...');
}
function calculate() {
    var a = document.getElementById('d1');
    var b = document.getElementById('d2');
    var c = document.getElementById('d3');
    var l1 = document.getElementById('l1');
    var l2 = document.getElementById('l2');
    var x = +a.value;
    var y = +b.value;
    var z = +c.value;
    l2.innerHTML = "";
    if ((x == y) && (y == z)) {
        l1.innerHTML = "It is an equilateral triangle.";
    }
    else if ((x == y) || (y == z) || (x == z)) {
        l1.innerHTML = "It is an isosceles triangle.";
        if ((Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2)) || (Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2)) || (Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2)))
            l2.innerHTML = "It is also a right angle triangle.";
    }
    else if ((x != y) && (y != z) && (x != z)) {
        l1.innerHTML = "It is an scalene triangle.";
        if ((Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2)) || (Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2)) || (Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2)))
            l2.innerHTML = "It is also a right angle triangle.";
    }
}
function findRealAndImag() {
    var a = document.getElementById('d1');
    var b = document.getElementById('d2');
    var c = document.getElementById('d3');
    var data = a.value;
    var real;
    var imag;
    var i = data.indexOf("+");
    if (i != -1) {
        real = +data.substring(0, i);
        imag = +data.substring(i + 1, data.length - 1);
        b.value = "Real Part: " + real;
        c.value = "Imag Part: " + imag;
    }
    else {
        real = +data.substring(0, data.length);
        b.value = "Real Part: " + real;
        c.value = "Imag Part: 0";
    }
}
function for_loop() {
    var count = 1;
    for (; count <= 100; count++) {
        console.log(count);
    }
}
function while_loop() {
    var count = 100;
    while (count > 0) {
        console.log(count);
        count--;
    }
}
function doWhile_loop() {
    var count = 100;
    do {
        console.log(count);
        count -= 10;
    } while (count > 0);
}
function findFactorial() {
    var input = document.getElementById('input');
    var result = document.getElementById('result');
    var num = +input.value;
    var count = 2;
    var fact = 1;
    while (count <= num) {
        fact *= count;
        count++;
    }
    result.value = "Factorial of " + num.toString() + " is " + fact.toString();
}
function dynamic() {
    var input = document.getElementById('input');
    var table = document.getElementById('table_1');
    var count = 1;
    var num = +input.value;
    while (table.rows.length > 1) {
        table.deleteRow(1);
    }
    for (count = 1; count <= num; count++) {
        var row = table.insertRow();
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = count.toString();
        cell.appendChild(text);
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = (count * count).toString();
        cell.appendChild(text);
    }
}
function dynamic2() {
    var input = document.getElementById('input');
    var table = document.getElementById('table_1');
    var count = 1;
    var num = +input.value;
    while (table.rows.length > 1) {
        table.deleteRow(1);
    }
    for (count = 1; count <= num; count++) {
        var row = table.insertRow();
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.id = "t" + count;
        text.style.textAlign = "center";
        text.style.background = "yellow";
        text.value = count.toString();
        cell.appendChild(text);
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.id = "tt" + count;
        text.style.textAlign = "center";
        text.style.background = "yellow";
        cell.appendChild(text);
    }
}
function squares() {
    var table = document.getElementById('table_1');
    var count = 1;
    while (count < table.rows.length) {
        var row_num = document.getElementById("t" + count);
        var num = +row_num.value;
        var square_num = document.getElementById("tt" + count);
        square_num.value = (num * num).toString();
        count++;
    }
}
