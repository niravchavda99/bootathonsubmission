function strfn() {
    let input : HTMLInputElement = <HTMLInputElement>document.getElementById('input');
    let answer : HTMLInputElement = <HTMLInputElement>document.getElementById('answer');

    var b: string = input.value;
    var c : string;
    var arr : string[];
    var i : number;

    answer.innerHTML = "<b>The orignal string is: </b>" + b;

    c = b.substring(8, 12);
    answer.innerHTML += "<br>Substring 8, 12: " + c;
    
    c = b.charAt(0);
    answer.innerHTML += "<br>charAt: " + c;

    c = b.concat(" is Fun");
    answer.innerHTML += "<br>concat: " + c;

    arr = b.split(" ");
    answer.innerHTML += "<br>arr[0]: " + arr[0];
    answer.innerHTML += "<br>arr[1]: " + arr[1];
    answer.innerHTML += "<br>arr[2]: " + arr[2];

    i = b.indexOf("t");
    answer.innerHTML += "<br>index of t: " + i.toString();

    c = b.toUpperCase();
    answer.innerHTML += "<br>uppercase: " + c;
}