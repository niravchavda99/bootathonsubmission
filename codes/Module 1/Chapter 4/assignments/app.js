var n1 = document.getElementById('n1'); //Number 1
var n2 = document.getElementById('n2'); //Number 2
var n3 = document.getElementById('n3'); //Answer Field
//Function to check if input is valid or not
function isValid(num) {
    if (num == null) //Return false if input is provided
        return false;
    if (isNaN(num)) //Return false if input is Not a Number
        return false;
    return true; //Return true if input is valid
}
//Function to add two input numbers
function add() {
    var a = parseFloat(n1.value); //Value of Input Field 1
    var b = parseFloat(n2.value); //Value of Input Field 2
    if (!isValid(a)) { //Return Error if Number 1 is not valid
        alert('Number 1 is not a number!');
        return;
    }
    if (!isValid(b)) { //Return Error if Number 2 is not valid
        alert('Number 2 is not a number!');
        return;
    }
    var c = a + b; //Add Number 1 and Number 2
    n3.value = c.toString(); //Set the answer field with the result
}
function subtract() {
    var a = parseFloat(n1.value); //Value of Input Field 1
    var b = parseFloat(n2.value); //Value of Input Field 2
    if (!isValid(a)) { //Return Error if Number 1 is not valid
        alert('Number 1 is not a number!');
        return;
    }
    if (!isValid(b)) { //Return Error if Number 2 is not valid
        alert('Number 2 is not a number!');
        return;
    }
    var c = a - b; //Subtract Number 2 from Number 1
    n3.value = c.toString(); //Set the answer field with the result
}
function multiply() {
    var a = parseFloat(n1.value); //Value of Input Field 1
    var b = parseFloat(n2.value); //Value of Input Field 2
    if (!isValid(a)) { //Return Error if Number 1 is not valid
        alert('Number 1 is not a number!');
        return;
    }
    if (!isValid(b)) { //Return Error if Number 2 is not valid
        alert('Number 2 is not a number!');
        return;
    }
    var c = a * b; //Multipy Number 1 and Number 2
    n3.value = c.toString(); //Set the answer field with the result
}
function divide() {
    var a = parseFloat(n1.value); //Value of Input Field 1
    var b = parseFloat(n2.value); //Value of Input Field 2
    if (!isValid(a)) { //Return Error if Number 1 is not valid
        alert('Number 1 is not a number!');
        return;
    }
    if (!isValid(b)) { //Return Error if Number 2 is not valid
        alert('Number 2 is not a number!');
        return;
    }
    var c = a / b; //Divide Number 1 by Number 2
    n3.value = c.toString(); //Set the answer field with the result
}
