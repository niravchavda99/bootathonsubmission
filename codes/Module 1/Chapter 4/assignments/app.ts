let n1 : HTMLInputElement = <HTMLInputElement> document.getElementById('n1'); //Number 1
let n2 : HTMLInputElement = <HTMLInputElement> document.getElementById('n2'); //Number 2
let n3 : HTMLInputElement = <HTMLInputElement> document.getElementById('n3'); //Answer Field

//Function to check if input is valid or not
function isValid(num:number) {
    if(num == null) //Return false if input is provided
        return false;
    if(isNaN(num)) //Return false if input is Not a Number
        return false;
    return true; //Return true if input is valid
}

//Function to add two input numbers
function add() {
    
    let a : number = parseFloat(n1.value); //Value of Input Field 1
    let b : number = parseFloat(n2.value); //Value of Input Field 2
    
    if(!isValid(a)) { //Return Error if Number 1 is not valid
        alert('Number 1 is not a number!');
        return;
    }
    if(!isValid(b)) { //Return Error if Number 2 is not valid
        alert('Number 2 is not a number!');
        return;
    }
    let c : number = a + b; //Add Number 1 and Number 2
    n3.value = c.toString(); //Set the answer field with the result
}

function subtract() {
     
    let a : number = parseFloat(n1.value); //Value of Input Field 1
    let b : number = parseFloat(n2.value); //Value of Input Field 2
    
    if(!isValid(a)) { //Return Error if Number 1 is not valid
        alert('Number 1 is not a number!');
        return;
    }
    if(!isValid(b)) { //Return Error if Number 2 is not valid
        alert('Number 2 is not a number!');
        return;
    }
    let c : number = a - b; //Subtract Number 2 from Number 1
    n3.value = c.toString(); //Set the answer field with the result
}

function multiply() {
     
    let a : number = parseFloat(n1.value); //Value of Input Field 1
    let b : number = parseFloat(n2.value); //Value of Input Field 2
    
    if(!isValid(a)) { //Return Error if Number 1 is not valid
        alert('Number 1 is not a number!');
        return;
    }
    if(!isValid(b)) { //Return Error if Number 2 is not valid
        alert('Number 2 is not a number!');
        return;
    }
    let c : number = a * b; //Multipy Number 1 and Number 2
    n3.value = c.toString(); //Set the answer field with the result
}

function divide() {
    
    let a : number = parseFloat(n1.value); //Value of Input Field 1
    let b : number = parseFloat(n2.value); //Value of Input Field 2
    
    if(!isValid(a)) { //Return Error if Number 1 is not valid
        alert('Number 1 is not a number!');
        return;
    }
    if(!isValid(b)) { //Return Error if Number 2 is not valid
        alert('Number 2 is not a number!');
        return;
    }
    let c : number = a / b; //Divide Number 1 by Number 2
    n3.value = c.toString(); //Set the answer field with the result
}